from bs4 import BeautifulSoup
import urllib
import time
import os
import cv2

def get(amount=1, start_num=1):
    base_url = "http://10000img.com/"
    url = "http://10000img.com/ran.php"
    start = time.time()

    count = start_num
    error = 0
    while count <= count + amount:
	try:
        	print("+----------------[%dth image] ------------------+" % count)
        	html = urllib.urlopen(url)
        	source = html.read()

        	soup = BeautifulSoup(source, "html.parser")
        	img = soup.find("img")
        	img_src = img.get("src")
        	img_url = base_url + img_src
        	img_name = "../../images/" + "{:04d}".format(count) + ".jpg"
        	print("image:", img_url)
        	urllib.urlretrieve(img_url, img_name)
	except KeyboardInterrupt:
		print("\rEnd Crawling")
		return;
	except:
		print("Error " + str(error) + " occurred.")
	finally:
		count += 1
    print("Total number of errors: " + str(error))

get(2000, 2001)
