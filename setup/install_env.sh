sudo mkdir /opt/mpi
sudo mkdir /opt/mpi-dl
sudo mkdir /opt/mpi-build
sudo apt install gfortran
cp ~/parallel_computing/assets/mpich-3.3.tar.gz /opt/mpi-dl
cd /opt/mpi-dl
tar zxvf mpich-3.3.tar.gz
cd /opt/mpi-build
sudo /opt/mpi-dl/mpich-3.3/configure --prefix=/opt/mpi
cp ~/parallel_computing/assets/topology-opencl.c /opt/mpi-dl/mpich-3.3/src/hwloc/hwloc/topology-opencl.c
cp ~/parallel_computing/assets/topology-opencl.c /opt/mpi-dl/mpich-3.3/src/pm/hydra/tools/topo/hwloc/hwloc/hwloc/topology-opencl.c
sudo make
sudo make install
