from mpi4py import MPI
#import numpy as np
import csv
import socket
#import pandas as pd
from datetime import datetime
from time import time, sleep

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#rank = str(int(socket.gethostname()[-1])-1)
#size = 4

sendbuf = None
data_array = None

chunk_size = 1000

start_time = time()

def chunks(l, n):
	for i in range(0, len(l), n):
		yield l[i:i + n]

if rank == 0:
	with open('parallel_computing/assets/Data.csv', newline='') as csvfile:
		data_iter = csv.reader(csvfile, delimiter=' ')
		data = [data[0] for data in data_iter][:200000]
		data_size = len(data)
	node_size = size - 1
	print(node_size, size)
	title = data.pop(0)
	data = list(chunks(data, int(data_size/node_size) + node_size))
	data = [[title]] + data
	#df_iter = pd.read_csv('parallel_computing/Data.csv', chunksize=chunk_size, iterator = True)

	#for iter_num, chunk in enumerate(df_iter, 1):
	#	print(f'Processing iteration {iter_num}')
	#	print(chunk)
else:
	data = None

print(socket.gethostname() + ' -> rank ' + str(rank))
data = comm.scatter(data, root=0)
print('number of data on ' + str(rank) + ': ', str(len(data)))

if rank != 0:
	print('Calculating...')
	data = [datetime.strptime(row, '%Y.%M') for row in data]
	print('toDatetime: ', rank, data[:3])

calculated = comm.gather(data, root=0)
if rank == 0:
	print('done, with' + str(len(calculated[1])) + 'results.')

end_time = time() - start_time
sleep(1)
print(str(rank), " finished on ", str(end_time) + "s.")
