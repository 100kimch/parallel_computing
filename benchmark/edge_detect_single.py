import cv2
from time import time

amount_image = 5000
start_num = 1

start_time = time()
def edge_detect(start_num, amount):
	for i in range(start_num, start_num + amount):
		img = cv2.imread("../../images/" + str("{:04d}".format(i)) + ".jpg")
		edges = cv2.Canny(img, 200, 300)
		fname = "../../images/edge-detected/" + str("{:04d}".format(i)) + "-edge.jpg"
		cv2.imwrite(fname, edges)
		# for python 3
		#print(fname + " saved.  \r", end='')
		# for python 2.7
		print fname, " saved. \r",
		i += 1

edge_detect(start_num, amount_image)
end_time = time() - start_time
print("finished on " + str(end_time) + "s.")
