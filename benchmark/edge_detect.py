from mpi4py import MPI
import cv2
import time
import socket
from time import time, sleep

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

amount_image = 2000
chunk_size = 500

start_time = time.time()

def chunks(l, n):
	for i in range(0, len(l), n):
		yield l[i:i + n]

def edge_detect(start_num, amount):
	for i in range (1,21):
		edge = cv2.imread("../../images/"+str("{:04d}".format(i))+'.jpg')
		edges=cv2.Canny(edge,200,300)
		fname="images/edge-detected"+str(i)+'-edge.jpg'
		cv2.imwrite(fname)
		# cv2.imshow("abc",fname)
		i += 1
edge_detect()

if rank == 0:
	for i in range(1, amount_image + 1):
		target = cv2.imread("../../images/" + str("{:04d}".format(i)) + '.jpg')
		edge_target = cv2.Canny(edge, 200, 300)
		fname = "../../edge-images/" + str("{:04d}".format(i)) + '.jpg')
		cv2.imwrite(fname)
		i += 1
else:
	data = None




end_time = time() - start_time
print(str(rank) + "finished on " + str(end_time) + "s.")
