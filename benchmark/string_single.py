#import numpy as np
import csv
#import pandas as pd
from datetime import datetime
from time import time, sleep

#rank = str(int(socket.gethostname()[-1])-1)
#size = 4

sendbuf = None
data_array = None

chunk_size = 1000

start_time = time()

def chunks(l, n):
	for i in range(0, len(l), n):
		yield l[i:i + n]

with open('parallel_computing/assets/Data.csv', newline='') as csvfile:
	data_iter = csv.reader(csvfile, delimiter=' ')
	data = [data[0] for data in data_iter][:200000]
	data.pop(0)
	data_size = len(data)
#node_size = size - 1
#print(node_size, size)
#title = data.pop(0)
#data = list(chunks(data, int(data_size/node_size) + node_size))
#data = [[title]] + data
#df_iter = pd.read_csv('parallel_computing/Data.csv', chunksize=chunk_size, iterator = True)

#for iter_num, chunk in enumerate(df_iter, 1):
#	print(f'Processing iteration {iter_num}')
#	print(chunk)
#print(socket.gethostname() + ' -> rank ' + str(rank))
#data = comm.scatter(data, root=0)
#print('number of data on ' + str(rank) + ': ', str(len(data)))

#print(datetime.strptime(data[0], '%Y.%M'))
print(data[:2])
data = [datetime.strptime(row, '%Y.%M') for row in data]
print('toDatetime: ', data[:3])
print('done, with' + str(len(data)) + 'results.')

end_time = time() - start_time
sleep(1)
print("finished on ", str(end_time) + "s.")
